/*
 * @author Jack Hoang
 * Student Number: 991495093 
 * 
 *  Used as JUnit class to test methods from base class
 *  Used for testing exercise with JUnit
 */

package password;

import static org.junit.Assert.*;

import org.junit.Test;

import password.PasswordValidator;

public class PasswordValidatorTest {

	@Test
	public void testIsValidlengthRegular() {
		boolean result = PasswordValidator.isValidLength("abcdeef12");
		assertTrue("password does not meet length requirement", result);
	}
	
	@Test
	public void testIsValidlengthException() {
		boolean result = PasswordValidator.isValidLength("");
		assertFalse("password does not meet length requirement", result);
	}
	
	@Test
	public void testIsValidlengthBoundaryIn() {
		boolean result = PasswordValidator.isValidLength("test1234");
		assertTrue("password does not meet length requirement", result);
	}
	
	@Test
	public void testIsValidlengthBoundaryOut() {
		boolean result = PasswordValidator.isValidLength("test123");
		assertFalse("password does not meet length requirement", result);
	}

	@Test
	public void testIsValidDigitsRegular() {
		boolean result = PasswordValidator.isValidDigits("test1234");
		assertTrue("password does not contain valid amount of digits", result);
	}
	
	@Test
	public void testIsValidDigitsException() {
		boolean result = PasswordValidator.isValidDigits("test");
		assertFalse("password does not contain valid amount of digits", result);
	}
	
	@Test
	public void testIsValidDigitsBoundaryIn() {
		boolean result = PasswordValidator.isValidDigits("tester12");
		assertTrue("password does not contain valid amount of digits", result);
	}
	
	@Test
	public void testIsValidDigitsBoundaryOut() {
		boolean result = PasswordValidator.isValidDigits("test1");
		assertFalse("password does not contain valid amount of digits", result);
	}

}
