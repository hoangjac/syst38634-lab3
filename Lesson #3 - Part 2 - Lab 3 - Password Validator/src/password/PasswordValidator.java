package password;

public class PasswordValidator {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	public static boolean isValidLength(String password) {
		
		//Check password length meets minimum requirement
		if(password.length() >= 8) {
			return true; 
		} else {
			return false;
		}
		
	}
	
	public static boolean isValidDigits(String password) {
		
		//Init variables
		int count = 0;
		char character;
		
		//Count number of digits in the password
		for(int i = 0; i< password.length(); i++) {
			
			//Check each character if it is a digit
			character = password.charAt(i);
			
			if(Character.isDigit(character)) {
				count++;
			}
		
		}
		
		//if count is 2 or more, return true that there are enough digits in password
		if (count >= 2) {
			return true;
		} else {
			return false;
		}
		
	}

}
